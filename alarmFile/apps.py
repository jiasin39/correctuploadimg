from django.apps import AppConfig


class AlarmfileConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'alarmFile'
