const image_input = document.querySelector("#image_input");
var upload_image = "";

image_input.addEventListener("change", function(){
    const render = new FileReader("load", () => {
        upload_image = render.result;
        document.querySelector("#display_image").styles.backgroundImage = `url(${upload_image})`;
    });
    render.readAsDataURL(this.files[0]);
})